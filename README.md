# unraid-claim-soundcards

Install: https://forums.unraid.net/topic/48286-plugin-ca-user-scripts/ to add own userscripts and schedule it for "array start", then create a script with `grab-soundcard.sh` contents.

Your VM xmls ( located in: `/etc/libvirt/qemu/`) should contain following passthrough to be replaced by the script, note that the `<address type='usb' bus='0' port='1'/>` should be unique, if there is more USB passthroughs.

```
<hostdev mode='subsystem' type='usb' managed='no'>
  <source>
    <address bus='3' device='25'/>
  </source>
  <address type='usb' bus='0' port='1'/>
</hostdev> 
```

Remember to change the variables inside `grab-soundcard.sh`:
```
vm1_name="Arch"
vm2_name="Windows 10"
soundcard="C-Media"
```