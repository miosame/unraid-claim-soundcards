#!bin/bash

#change variables here only, examples given as VM1 "Arch" and VM2 "Windows 10", with sound cards from "C-Media" (check lsusb to verify)
vm1_name="Arch"
vm2_name="Windows 10"
soundcard="C-Media"

#get bus and device ids
sound_bus=$(lsusb | grep C-Media | sed -e 's/^Bus \([0-9]*\)\(.*\)$/\1/' | sed -n 1p)
sound_device1=$(lsusb | grep $soundcard | sed -e 's/\(.*\)Device \([0-9]*\)\(.*\)$/\2/' | sed -n 1p)
sound_device2=$(lsusb | grep $soundcard | sed -e 's/\(.*\)Device \([0-9]*\)\(.*\)$/\2/' | sed -n 2p)

#remove leading zeros
sound_bus=$(echo $sound_bus | sed -e 's/\(^\|[^[:digit:]]\+\)0\+\([[:digit:]]\)/\1\2/g')
sound_device1=$(echo $sound_device1 | sed -e 's/\(^\|[^[:digit:]]\+\)0\+\([[:digit:]]\)/\1\2/g')
sound_device2=$(echo $sound_device2 | sed -e 's/\(^\|[^[:digit:]]\+\)0\+\([[:digit:]]\)/\1\2/g')

#replace the usb passthrough inside the VM xmls
sed -i.bak "s/<address bus='\([0-9]*\)' device='\([0-9]*\)'\/>/<address bus='$sound_bus' device='$sound_device1'\/>/" "/etc/libvirt/qemu/$vm1_name.xml"
sed -i.bak "s/<address bus='\([0-9]*\)' device='\([0-9]*\)'\/>/<address bus='$sound_bus' device='$sound_device2'\/>/" "/etc/libvirt/qemu/$vm2_name.xml"